words="bunker face olive beef total desert genius boat salad harsh ostrich setup screen system move hammer bachelor farm source connect bus dog side blur"

import binascii
from mnemonic import mnemonic
from bip32 import BIP32
mnemo = mnemonic.Mnemonic('english')
seed = mnemo.to_seed(words)

bip32 = BIP32.from_seed(seed)

first_btc_wallet_first_address_pub_key=bip32.get_pubkey_from_path("m/44'/0'/0'/0/0")

print(binascii.hexlify(first_btc_wallet_first_address_pub_key))
